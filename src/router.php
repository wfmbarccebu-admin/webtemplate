<?php

header("Content-Security-Policy: default-src 'self' 'unsafe-inline'; img-src 'self'; frame-ancestors 'none'");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");

include_once(_DRS_.'/forms/functions.php');

$_GET['param'] = isset($_GET['param'])?$_GET['param']:'dashboard';
$params = explode('/',$_GET['param']);
$category = $params[0];
$page = isset($params[1])?$params[1]:$params[0];
$tr_route = $page==$category?'main':$category;
$allowed_ow_pages = array('login','logout','masterlogout','err','error','maintenance');

include_once(__DIR__.'/builder/layout.php');
