<?php

header("Content-Security-Policy: default-src 'self' 'unsafe-inline'; img-src 'self'; frame-ancestors 'none'");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");

include_once(_DRS_.'/forms/functions.php');

$_GET['param'] = isset($_GET['param'])?$_GET['param']:'dashboard';
$params = explode('/',$_GET['param']);
$category = $params[0];
$page = isset($params[1])?$params[1]:$params[0];
$tr_route = $page==$category?'pages':$category;
$emp_id = isset($_GET['employee_ident'])?$_GET['employee_ident']:(isset($_SESSION['ident'])?$_SESSION['ident']:'1');
$month = isset($_GET['month'])?$_GET['month']:$cfg['target'];
$month = date("Y-m-d",strtotime("last day of this month", strtotime($month)));
$profile = isset($_GET['employee_ident'])?"?month={$month}&employee_ident={$emp_id}":"";
$allowed_ow_pages = array('login','logout','masterlogout','err','error','maintenance');

if(!isset($_SERVER['HTTP_REFERER']) AND !in_array($page, $allowed_ow_pages) AND !isset($_SESSION['login'])){
  if($page == 'dashboard' AND (!isset($_SESSION['login']) OR (isset($_SESSION['login']) AND $_SESSION['login']!=1))){

      if(isset($_GET['returnTo'])){
        $authSanitizer = new Sanitizer();
        $returnTo = $_GET['returnTo']!=""?'/'.urldecode($_GET['returnTo']):'';
        $checkReturnAddress = $returnTo==''?0:$authSanitizer->isFlagged($returnTo);
        if($checkReturnAddress == 0){
          header('location: '._DR_.'/login?returnTo='.$_GET['returnTo']);
        }else{
          header('location:'._DR_.'/login');          
        }
      }else{
        header('location:'._DR_.'/login');
      }
      
  }else{
    include_once(_DRS_.'/builder/500.php');
    exit(); 
  }
}else{

  if($page=='form'){
    include_once(_DRS_.'/forms/router.php');
    exit();   
  }
  if($page=='err' OR $page=='error'){
    include_once(_DRS_.'/builder/500.php');
    exit();
  }

  $ua = getBrowser();
  if(($ua['name'] == 'Edge' AND $ua['version'] >= 79) OR $ua['name'] == 'Mozilla Firefox' OR $ua['name'] == 'Chrome' OR $ua['name'] == 'Google Chrome'){
    echo "";
  }else{
    include_once(_DRS_.'/builder/unsupported.php');
    exit();    
  }
  
  if($page=='masterlogout' OR ($page=='logout' AND isset($_GET['pkc']))){
    if(isset($_GET['pkc'])){
      $pkc = base64_decode($_GET['pkc']);
      if(date('i', time() - $pkc ) > 5){
        echo $msg['invalid-token'];
        exit();
      }
    }
    session_destroy();
    header('location:'._DR_.'/login');
    exit();    
  }
  if($page=='login' AND !isset($_SESSION['login'])){
  	if(isset($_POST['login'])){
  		include_once(_DRS_.'/forms/authenticate.php');
  	}else{
  		include_once(_DRS_.'/pages/login.php');
  	}
  	exit();
  }
  if(($page=='logout' OR !isset($_SESSION['login']) OR (isset($_SESSION['login']) AND $_SESSION['login']!=1)) AND !isset($_GET['pkc'])){
    $qryS = substr($_SERVER["QUERY_STRING"],6);
    $qrySs = ($qryS=='' OR $qryS=='logout')?'':'?returnTo='.urlencode($qryS);

    if(!isset($_SESSION['lockedOutTime'])){
      session_destroy();
      if(isset($_SESSION['lockedOutTime'])){unset($_SESSION['lockedOutTime']);}
      if(isset($_SESSION['attempt'])){unset($_SESSION['attempt']);}
    }
    header('location:'._DR_.'/login'.$qrySs);
    exit();      
  }


/* Start Sanitizing Inputs */
  $sanitize = new Sanitizer();
  $checkInput = array();
  $checkInput['category'] = $sanitize->isPath($category);
  $checkInput['page'] = $sanitize->isPath($page);
  $checkInput['emp_id'] = $sanitize->isNumber($emp_id);
  $checkInput['month'] = $sanitize->isDate($month);
  $checkInput['frmType'] = isset($_POST['frmType'])?$sanitize->isAlphaNum($_POST['frmType']):0;
  $checkInput['report_type'] = isset($_POST['report_type'])?$sanitize->isDate($_POST['report_type']):0;
  $checkInput['frmAction'] = isset($_POST['frmAction'])?$sanitize->isAlphaNum($_POST['frmAction']):0;

  $checkInput['director'] = isset($_GET['director'])?$sanitize->isNumber($_GET['director']):0;
  $checkInput['view'] = isset($_GET['view'])?(($page=='assay' OR $page=='nps')?$sanitize->isDate($_GET['view']):$sanitize->isLetters($_GET['view'])):0;
  $checkInput['viewMonth'] = isset($_GET['month'])?$sanitize->isDate($_GET['month']):0;

  if(array_sum($checkInput) > 0){
    $forceError = 'true';
    include_once(_DRS_.'/builder/layout.php');
    exit();
  }
/* End Sanitizing */
  if(!is_numeric($emp_id)){
    header('location:'._DR_.'/error');    
  }else{
    if($page == 'login'){
      header('location:'._DR_.'/error');
    }else{
      if(isset($_GET['returnTo'])){
        $authSanitizer = new Sanitizer();
        $returnTo = $_GET['returnTo']!=""?'/'.urldecode($_GET['returnTo']):'';
        $checkReturnAddress = $returnTo==''?0:$authSanitizer->isFlagged($returnTo);
        if($checkReturnAddress == 0){
          header('location: '._DR_.$returnTo);
        }else{
          header('location:'._DR_.'/error');
        }
      }else{
        include_once(__DIR__.'/builder/layout.php');      
      }
    }  
  }

}