<?php
  if(isset($forceError)){
    include_once(__DIR__.'/500.php');
    exit;
  }
  if($tr_route=='uimanager'){
    include_once(_DRS_.'/admin/router.php');
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?php echo _DR_?>/dist/img/TP-logo.ico"> 
  <title><?php echo getLocalSettings('siteName'); ?></title>

  <link rel="stylesheet" href="<?php echo _DR_ ?>/dist/css/style.css">
  <script src="<?php echo _DR_ ?>/dist/js/jquery.min.js"></script>
  <script src="<?php echo _DR_ ?>/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo _DR_ ?>/dist/js/adminlte.min.js"></script>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<?php include_once(__DIR__.'/loader.php')?>
<div class="wrapper">
  <?php 
    include_once(__DIR__.'/header.php');
    include_once(__DIR__.'/sidebar.php');
  ?>
  <div class="content-wrapper">
    <?php
      include_once(__DIR__.'/title.php');

      $pg_route = $tr_route=='disputes'?'reports':$tr_route;
      
      if($pg_route=='main'){
        include_once(_DRS_.'/main/router.php');        
      }elseif(is_dir(_DRS_.'/main/'.$pg_route)){
        include_once(_DRS_.'/main/'.$pg_route.'/router.php');
      }else{
        include_once(__DIR__.'/404.php');
      }

    ?>
  </div>
  <?php 
    include_once(__DIR__.'/footer.php');
  ?>
</div>
<script>
  $(document).ready(()=>{$('.loading-screen').hide()})
</script>
</body>
</html>