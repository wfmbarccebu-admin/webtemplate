<?php

class mckQAConn{
    public function tryConnect(){
        try {
            // $DBH = new PDO("odbc:Driver={SQL Server};Server=mck-devdb-01;Database=WFM_Reports_Tracker;Uid=TPPH_WFM_AppUser;Pwd=M8jbYuWU*72021-D3V;");
            $DBH = new PDO("sqlsrv:Server=mck-devdb-01;Database=WFM_Reports_Tracker", "TPPH_WFM_AppUser", "M8jbYuWU*72021-D3V");
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $DBH;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function tryMck(){
        try {
            // $DBH = new PDO("odbc:Driver={SQL Server};Server=mckwfmdb01.teleperformanceusa.com;Database=CCMS;Uid=TPPH_WFM_AppUser;Pwd=M8jbYuWU*7;");
            $DBH = new PDO("sqlsrv:Server=mckwfmdb01.teleperformanceusa.com;Database=WFM_Reports_Tracker", "TPPH_WFM_AppUser", "M8jbYuWU*7");
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $DBH;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function tryQADB(){
        try {
            // $DBH = new PDO("odbc:Driver={SQL Server};Server=mck-wfmsqlqa-01.teleperformanceusa.com;Database=CCMS;Uid=TPPH_WFM_AppUser;Pwd=M8jbYuWU*72021;");
            $DBH = new PDO("sqlsrv:Server=mck-wfmsqlqa-01.teleperformanceusa.com;Database=WFM_Reports_Tracker", "TPPH_WFM_AppUser", "M8jbYuWU*72021");
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $DBH;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function localDB(){
        try{
            $DBH = new PDO("sqlite:"._DR_."/localDB.sqlite");
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $DBH;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}