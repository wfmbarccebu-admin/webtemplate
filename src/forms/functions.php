<?php
if(!session_id()){session_start();}
date_default_timezone_set('Asia/Manila');
include_once(_DRS_."/config.php");
include_once(__DIR__."/classes/checkbrowser.php");

/*
error_reporting(0);
register_shutdown_function('errorHandler');

function errorHandler() {
   $err = error_get_last();
   if($err){
        include_once(_DRS_."/builder/500.php");
   }
}
*/

function getRealHost(){
   list($realHost,)=explode(':',$_SERVER['HTTP_HOST']);
   return $realHost;
}
function SQL_enc($q){
    return htmlspecialchars($q, ENT_QUOTES);
}
function SQL_dec($q){
    return htmlspecialchars_decode($q, ENT_QUOTES);
}

spl_autoload_register(function($className) {
    $file = __DIR__ . "\\classes\\" . $className . '.php';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
    if (file_exists($file)) {
        include $file;
    }
});

