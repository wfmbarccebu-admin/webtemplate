<?php
$frmName = isset($_POST['frmType'])?$_POST['frmType']:(isset($_GET['frmType'])?$_GET['frmType']:'');

if(isset($_GET['download'])){
	include_once(__DIR__.'/downloads/router.php');
	exit();
}
if(!file_exists(__DIR__."/forms/{$frmName}.php")){
	header('location: '._DR_.'/error');
}else{
	include_once(__DIR__."/forms/{$frmName}.php");
}